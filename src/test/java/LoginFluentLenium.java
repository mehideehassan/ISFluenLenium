import org.fluentlenium.adapter.testng.FluentTestNg;
import org.openqa.selenium.By;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

import static org.assertj.core.api.Assertions.assertThat;


/**
 * Created by mehidee on 12/7/16.
 */
public class LoginFluentLenium extends FluentTestNg {



    @Test
    public void test() throws InterruptedException {
        goTo("http://www.ideascale.me/a/login");
        $("#login-email").fill().with("adib@yahoo.com");
        $("#login-password").fill().with("!A123456");


        $(By.cssSelector("button[class='btn btn-primary disabled-when-loading']")).click();
        await().explicitlyFor(3, TimeUnit.SECONDS);
        assertThat(window().title()).contains("Your Communities");
    }
}